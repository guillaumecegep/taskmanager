import React from 'react'
import Task from './Task'

const TasksList = ({ tasks }) => {
  return (
    <div id="task-list">
      {tasks.map((title, i) => {
        return (
          <Task
            key={i}
            id={tasks[i].ID}
            userId={tasks[i].post_author}
            title={tasks[i].post_title}
            status={tasks[i].post_status}
          />
        )
      })}
    </div>
  )
}

export default TasksList
