import React from 'react'
import { Link } from 'gatsby'

class Navi extends React.Component {
  render() {
    const { location, title } = this.props
    return (
      <nav className="navbar navbar-expand navbar-dark flex-column flex-md-row bg-primary">
        <div className="container">
          <Link className="text-center" to="/">
            <h1 className="navbar-brand mb-0">{title}</h1>
          </Link>
          <div className="navbar-nav-scroll">
            <ul className="navbar-nav bd-navbar-nav flex-row">
              <li
                className={
                  location.pathname === '/gestionnaire/'
                    ? 'nav-item active'
                    : 'nav-item'
                }
              >
                <Link to="/gestionnaire/" className="nav-link">
                  Gestionnaire de tâches
                </Link>
              </li>
              <li
                className={
                  location.pathname === '/docs/'
                    ? 'nav-item active'
                    : 'nav-item'
                }
              >
                <Link to="/docs/" className="nav-link">
                  Documentation
                </Link>
              </li>
              <li
                className={
                  location.pathname === '/contact/'
                    ? 'nav-item active'
                    : 'nav-item'
                }
              >
                <Link to="/contact/" className="nav-link">
                  Contact
                </Link>
              </li>
            </ul>
          </div>
          <div className="navbar-nav flex-row ml-md-auto d-none d-md-flex" />
        </div>
      </nav>
    )
  }
}

export default Navi
