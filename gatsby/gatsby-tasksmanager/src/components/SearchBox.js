import React from 'react'

const SearchBox = ({ searchfield, searchChange }) => {
  return (
    <div className="search-container py-4">
      <input
        className="searchfield"
        type="search"
        placeholder="Rechercher une tâche..."
        onChange={searchChange}
      />
    </div>
  )
}
export default SearchBox
