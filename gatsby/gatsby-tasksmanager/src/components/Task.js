import React from 'react'
import axios from 'axios'

//Placeholders modifiables
const HandleTitle = e => {
  document.getElementById('post_title_put').value = e.target.placeholder
}
const HandleUser = e => {
  document.getElementById('post_author_put').value = e.target.placeholder
}

//Modification d'une tâche
const handlePut = event => {
  event.preventDefault()

  const id = event.target.id

  let post_title = document.getElementById('post_title_put').value
  let post_author = document.getElementById('post_author_put').value
  let post_status_select = document.getElementById('post_status_put')
  let post_status =
    post_status_select.options[post_status_select.selectedIndex].value

  //Requête PUT à l'API Wordpress
  axios({
    method: 'PUT',
    url: 'https://ttm.gernst.ca/wp-json/tasksmanager/v0/tasks/' + id,
    data: {
      post_status: post_status,
      post_title: post_title,
      post_author: post_author,
    },
  }).then(function(res) {
    console.log(res)
    const data = JSON.parse(res.data)

    //Retour d'un message de confirmation
    alert('La tâche a bien été modifiée')
  })
}

//Supression d'une tâche
const handleDelete = async event => {
  event.preventDefault()
  console.log(event.target.id)
  const id = event.target.id

  //Requête DELETE à l'API Wordpress
  axios
    .delete(`https://ttm.gernst.ca/wp-json/tasksmanager/v0/tasks/${id}`)
    .then(res => {
      console.log(res)
      console.log(res.data)

      document.getElementById('form-' + id).style.display = 'none'
    })
}

const Task = ({ id, title, status, userId }) => {
  return (
    <form id={'form-' + id} className="form-tasks">
      <input
        type="text"
        id="post_title_put"
        placeholder={title}
        onClick={HandleTitle}
      ></input>
      <input
        type="number"
        id="post_author_put"
        placeholder={userId}
        onClick={HandleUser}
      ></input>

      <select id="post_status_put">
        <option value="draft">A faire</option>
        <option value="publish">En cours</option>
        <option value="trash">Terminé</option>
      </select>

      <button id={id} type="submit" onClick={handlePut}>
        Modifier
      </button>
      <button id={id} type="submit" onClick={handleDelete}>
        Delete
      </button>
    </form>
  )
}

export default Task
