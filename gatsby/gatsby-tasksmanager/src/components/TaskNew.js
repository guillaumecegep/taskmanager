import React from 'react'
import axios from 'axios'

//Création d'une tâche
const handlePost = async event => {
  event.preventDefault()

  let post_title = document.getElementById('titre_post').value
  let post_author = document.getElementById('author_post').value
  let post_status = document.getElementById('status_post').value
  const id = event.target.id

  //Requête POST à l'API Wordpress
  axios({
    method: 'post',
    url: 'https://ttm.gernst.ca/wp-json/tasksmanager/v0/tasks/',
    data: {
      post_status: post_status,
      post_title: post_title,
      post_author: post_author,
    },
  })
}

const TaskNew = () => {
  return (
    <form id="form-000" className="form-tasks">
      <input
        type="text"
        id="titre_post"
        placeholder="Entrez une nouvelle tâche"
        required
      ></input>
      <input
        type="number"
        id="author_post"
        placeholder="Sélectionner le id de l'utilisateur"
      ></input>
      <select id="status_post">
        <option value="draft">A faire</option>
        <option value="publish">En cours</option>
        <option value="trash">Terminé</option>
      </select>
      <button id="form-000" type="submit" onClick={handlePost}>
        Créer
      </button>
    </form>
  )
}

export default TaskNew
