import React, { Component } from 'react'
import get from 'lodash/get'
import Meta from 'components/Meta'
import Layout from 'components/Layout'

export class Contact extends Component {
  state = {
    name: '',
    message: '',
    email: '',
    sent: false,
    buttonText: 'Envoyer',
  }

  componentDidMount() {
    const script = document.createElement('script')
    console.log(script)

    script.src = 'js/script-contact.js'
    script.async = true

    document.body.appendChild(script)
  }

  resetForm = () => {
    this.setState({
      name: '',
      message: '',
      email: '',
      buttonText: 'Message Sent',
    })
  }

  formSubmit = e => {
    const contact = document.getElementById('contact-form')
    const submit = document.getElementById('btnSubmit')
    const name = document.getElementById('nom')
    const email = document.getElementById('courriel')
    const content = document.getElementById('form-content')
    e.preventDefault()

    submit.value = '...message en envoi'

    Email.send({
      // SecureToken: "e478b94c-4883-4156-9367-f89945fb9e3e",
      SecureToken: 'a279f9a6-689d-467f-a27c-2e32e8d8dee6',
      To: 'maxime.gendron.web@gmail.com',
      From: 'info@thetasksmananger.com',
      Subject: 'This is the subject',
      Body:
        'Nom: ' +
        name.value +
        ' <br> ' +
        'Courriel: ' +
        email.value +
        ' <br><br> ' +
        content.innerHTML,
    }).then(message => (submit.value = 'Message envoyé'))
  }

  render(data) {
    return (
      <Layout location="/contact/">
        <Meta site={get(data, 'site.meta')} />
        <div className="container">
          <div className="title">
            <h1>CONTACTEZ-NOUS</h1>
          </div>
          <form id="contact-form" onSubmit={this.formSubmit}>
            <div className="nom-courriel">
              <input
                type="text"
                name="nom"
                id="nom"
                placeholder="Nom"
                onChange={e => this.setState({ name: e.target.value })}
                value={this.state.name}
              />
              <input
                type="email"
                name="courriel"
                id="courriel"
                placeholder="Courriel"
                onChange={e => this.setState({ email: e.target.value })}
                required
                value={this.state.email}
              />
            </div>
            <textarea
              placeholder="Entrez votre message ici"
              name="message"
              id="form-content"
              type="text"
              onChange={e => this.setState({ message: e.target.value })}
              value={this.state.message}
              required
            ></textarea>
            <input type="submit" id="btnSubmit" value="Envoyer" />
          </form>
        </div>
      </Layout>
    )
  }
}
export default Contact
