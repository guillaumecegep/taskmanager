import React from 'react'
import get from 'lodash/get'
import Meta from 'components/Meta'
import Layout from 'components/Layout'

const Docs = ({ data }) => (
  <Layout location="/docs/">
    <Meta site={get(data, 'site.meta')} />
    <div className="container">
      <h1>Documentation</h1>
      <p>
        Taskmanager est un outil de <b>gestion de projet</b>. Ce plugin
        wordpress permet de créer et assigner des tâches à des particitpants.
      </p>

      <section>
        <h2>Architecture</h2>
        <p>
          Pour la réalisation de ce plugin, nous avons utilisé le plus possible
          l'architecture déjà présente dans Wordpress.
        </p>
      </section>

      <section>
        <h2>Bases de données</h2>
        <p>
          Lorsqu'une tâche est créer, une entrée est créé dans les table{' '}
          <b>tm_posts</b> et <b>tm_taskmanager_tasks</b>. Dans la table posts,
          on retrouve les informations de base tel que:
        </p>
        <ul>
          <li>ID</li>
          <li>post_id</li>
          <li>post_title</li>
          <li>post_status</li>
        </ul>
        <p>
          La table <b>tm_taskmanager_tasks</b> est utilisé pour marquer la
          relation entre la tâche et l'utilisateur. Elle comprend les éléments
          suivants:
        </p>
        <ul>
          <li>id</li>
          <li>post_id</li>
          <li>user_id</li>
        </ul>
      </section>

      <section>
        <h2>Fonctionnalités</h2>
        <h4>
          <b>URL</b>
        </h4>
        <code>/tasksmanager/v0/tasks</code>

        <h4 className="mt-4">
          <b>Method</b>
        </h4>
        <span className="badge badge-primary mr-2">GET</span>
        <span className="badge badge-primary mr-2">POST</span>
        <span className="badge badge-primary mr-2">PUT</span>
        <span className="badge badge-primary">DELETE</span>

        <h3 className="mt-4 mb-1">
          <b>GET</b>
        </h3>
        <p>Prendre tous les tâches</p>
        <h4>
          <b>URL Params</b>
        </h4>
        <code>/tasksmanager/v0/tasks</code>

        <p>Prendre une tâche avec son "ID"</p>
        <h4>
          <b>URL Params</b>
        </h4>
        <code>/tasksmanager/v0/tasks/:id</code>
        <h4>
          <b>Required</b>
        </h4>
        <code>id=[integer]</code>

        <h3 className="mt-4 mb-1">
          <b>POST</b>
        </h3>
        <p>Créer une tâche</p>
        <h4>
          <b>URL Params</b>
        </h4>
        <code>/tasksmanager/v0/tasks</code>

        <h3 className="mt-4 mb-1">
          <b>PUT</b>
        </h3>
        <p>Modifier une tâche</p>
        <h4>
          <b>URL Params</b>
        </h4>
        <code>/tasksmanager/v0/tasks/:id</code>
        <h4>
          <b>Required</b>
        </h4>
        <code>id=[integer]</code>

        <h3 className="mt-4 mb-1">
          <b>DELETE</b>
        </h3>
        <p>Supprimer une tâche</p>
        <h4>
          <b>URL Params</b>
        </h4>
        <code>/tasksmanager/v0/tasks/:id</code>
        <h4>
          <b>Required</b>
        </h4>
        <code>id=[integer]</code>
      </section>
    </div>
  </Layout>
)
export default Docs
