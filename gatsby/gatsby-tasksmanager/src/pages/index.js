import React from 'react'
import get from 'lodash/get'
import { Link } from 'gatsby'
import Meta from 'components/Meta'
import Layout from 'components/Layout'

const BlogIndex = ({ data, location }) => {
  const posts = get(data, 'remark.posts')
  return (
    <Layout location="/">
      <Meta site={get(data, 'site.meta')} />
      <div className="container">
        <h1>Taskmanager</h1>
        <p>
          Taskmanager est un outil de <b>gestion de projet</b>. Ce plugin
          wordpress permet de créer et assigner des tâches à des particitpants.
          Pour plus d'informations, voir la{' '}
          <Link to="/docs/" className="text-link">
            documentation
          </Link>
          .
        </p>

        <h2>L'équipe</h2>
        <p>Notre équipe se compose de 3 membres:</p>
        <ul>
          <li>Steven Drouin (a.k.a. Stonecold Steve Austin)</li>
          <li>Guillaume Ernst (a.k.a. Goldust)</li>
          <li>Maxime Gendron (a.k.a. Triple H)</li>
        </ul>
        <p>
          Les membres de l'équipe ont été impliqués dans toutes les étapes de la
          réalisation du plugin, ceci afin d'assurer une uniformité dans la
          structure et la logique du code.
        </p>
      </div>
    </Layout>
  )
}

export default BlogIndex
