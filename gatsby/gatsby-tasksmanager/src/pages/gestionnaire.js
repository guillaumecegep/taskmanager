import React, { Component } from 'react'
import axios from 'axios'
import TasksList from '../components/TasksList'
import Layout from 'components/Layout/index'
import Meta from 'components/Meta'
import get from 'lodash/get'
import TaskNew from 'components/TaskNew'
import SearchBox from 'components/SearchBox'

class Gestionnaire extends Component {
  constructor() {
    super()
    this.state = {
      tasks: [],
      searchfield: '',
      isLoading: true,
      error: null,
    }
  }

  componentDidMount() {
    axios
      .get('https://ttm.gernst.ca/wp-json/tasksmanager/v0/tasks/')

      .then(response => {
        this.setState({
          tasks: response.data,
          isLoading: false,
        })
      })
      .catch(error => this.setState({ error, isLoading: false }))
  }

  onSearchChange = e => {
    this.setState({ searchfield: e.target.value })
  }

  render(data) {
    // console.log(this.state)
    const { tasks, searchfield } = this.state
    const filteredTasks = tasks.filter(task => {
      return task.post_title.toLowerCase().includes(searchfield.toLowerCase())
    })

    if (!tasks.length) {
      return (
        <Layout location="/gestionnaire/">
          <Meta site={get(data, 'site.meta')} />
          <div className="container tasks-containers">
            <h1>Liste des tâches</h1>
            <TaskNew />
            <div className="spinner-border" role="status">
              <h2 className="sr-only">Chargement des tâches...</h2>
            </div>
          </div>
        </Layout>
      )
    } else {
      return (
        <Layout location="/gestionnaire/">
          <Meta site={get(data, 'site.meta')} />
          <div className="container tasks-containers">
            <h1>Liste des tâches</h1>
            <TaskNew />
            <SearchBox searchChange={this.onSearchChange} />

            <TasksList tasks={filteredTasks} />
          </div>
        </Layout>
      )
    }
  }
}

export default Gestionnaire
