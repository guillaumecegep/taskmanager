**Affiche une tache**
----
  Retourne une tâche en json.

* **URL**

  /tasksmanager/v0/tasks/:id

* **Method:**

  `GET`
  
*  **URL Params**

   **Required:**
 
   `id=[integer]`

* **Data Params**

  None

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{ id : 12, post_title : "Manger des patates" }`
 
* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
    **Content:** `{ error : "La tâche n'existe pas" }`

  OR

  * **Code:** 401 UNAUTHORIZED <br />
    **Content:** `{ error : "Vous n'êtes pas autoriser pour faire cette requête." }`

* **Sample Call:**

  ```javascript
    $.ajax({
      url: "/tasksmanager/v0/tasks/1",
      dataType: "json",
      type : "GET",
      success : function(r) {
        console.log(r);
      }
    });
  ```