<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'taskmanager' );

/** MySQL database username */
define( 'DB_USER', 'cegep' );

/** MySQL database password */
define( 'DB_PASSWORD', 'cegep123456' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

define('WP_HOME','https://ttm.gernst.ca/');
define('WP_SITEURL','https://ttm.gernst.ca/');


/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'e=OVvKf2:!0JW]Kp+BFHugO{Q*9j4;UWpO*Ay>di^QNzJ%p5z/3V7}#|bAt)%w/N' );
define( 'SECURE_AUTH_KEY',  '^.Mi_(BMY|K5_sh9ALM/|1YWpq2&22.B;{Ym%XBayFhuy>22`J?gh&tLvDhwSVD&' );
define( 'LOGGED_IN_KEY',    'EQ^?w+p)f2UzZ7&RcSy!!x][slPEy/UqW~R1<4*X{8#+Uo[rLIu(Fn*)<ntmSeKp' );
define( 'NONCE_KEY',        'Vp(kB<!K3^P<b=[+T@$g8S?j%pRH9e3jOdO-CC ^~>*i0r~d*T6bCx6DKu,#k?*w' );
define( 'AUTH_SALT',        '|Y.~a?@~6pc9b0alr!1jkeMIt2am?@)0uo=uxBvV|nZ^v=E8|C~& BEgQ=!x2%Rg' );
define( 'SECURE_AUTH_SALT', 'oG.k;;WLMa(U#.-0u:s5+&.C3jDoz-Z>JEj~FCH$-7q!_@;B3Aun//aB{ZG7Te%/' );
define( 'LOGGED_IN_SALT',   'XI0&L=: {M{U*NA)v6G_w#)<XJ{Ty11W9d!U gT{S#b?-.PI,C>~AQ33mUD$4qq6' );
define( 'NONCE_SALT',       'nx>v{?bczRsm`,n{(^bl<}2MHt8Aw-lEw{|sVhMcH?9Bb~33D!Yf/p[{)]};E7OV' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'tm_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', true );

define('WP_DEBUG_LOG', true);

define('WP_DEBUG_DISPLAY', false);



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
