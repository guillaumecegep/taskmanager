# Taskmanager Documentation

Taskmanager est un outil de **gestion de projet**. Ce plugin wordpress permet de créer et assigner des tâches à des particitpants.


# Architecture

Pour la réalisation de ce plugin, nous avons utilisé le plus possible l'architecture déjà présente dans Wordpress.

## Bases de données

Lorsqu'une tâche est créer, une entrée est créé dans les table **tm_posts** et **tm_taskmanager_tasks**. Dans la table posts, on retrouve les informations de base tel que:

- ID
- post_id
- post_title
- post_status

La table **tm_taskmanager_tasks** est utilisé pour marquer la relation entre la tâche et l'utilisateur. Elle comprend les éléments suivants:

- id
- post_id
- user_id


# Fonctionnalités

-   **URL**
    
    /tasksmanager/v0/tasks
    
-   **Method:**
        
    `GET` | `POST` | `DELETE` | `PUT`
   

## **GET**

> 	Prendre tous les tâches

 - **URL Params**
    
    /tasksmanager/v0/tasks

> 	Prendre une tâche avec son "ID"

 - **URL Params**
    
	/tasksmanager/v0/tasks/:id
    
    **Required:**
    
    id=[integer]

## **POST**
   > 	Créer une tâche
 - **URL Params**
    
	/tasksmanager/v0/tasks
    
    **Required:**
    
    id=[integer]
 
 ## **PUT**
   > 	Modifier une tâche
 - **URL Params**
    
	/tasksmanager/v0/tasks/:id
    
    **Required:**
    
    id=[integer]
 
 ## **DELETE**
   > 	Supprimer une tâche
 - **URL Params**
    
	/tasksmanager/v0/tasks/:id
    
    **Required:**
    
    id=[integer]
