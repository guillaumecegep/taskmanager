<?php


namespace Tasksmanager\Fields;

class Metaboxes
{
    public function __construct()
    {
        add_action( 'add_meta_boxes', [ $this, "add_tasks_textbox" ] );
        add_action( 'save_post_tasks', [ $this, "save_tasks_data" ] );
    }

    public function add_tasks_textbox()
    {
        add_meta_box(
            'tasks_box_id', // Unique ID
            __( 'Description tâche', 'tasks' ),  // Titre
            [ $this, 'tasks_metabox_html' ],  // Le callback
            'tasks'                   // Post type
        );
    }

    public function tasks_metabox_html( $post )
    {
        $value = get_post_meta( $post->ID, '_post_content', true );
        ?>
        <label for="post_content">Description de la tâche</label>
        <textarea name="post_content" id="post_content" class="widefat" cols="50" rows="5"><?php echo $value; ?></textarea>
        
        <?php
    }

    public function save_tasks_data( $post_id )
    {
        if( array_key_exists( 'post_content', $_POST ) ) {
            update_post_meta(
                $post_id,
                '_post_content',
                wp_filter_nohtml_kses( $_POST[ 'post_content' ] )
            );
        }
    }
}