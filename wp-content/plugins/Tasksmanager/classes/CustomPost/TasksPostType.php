<?php


namespace Tasksmanager\CustomPost;


class TasksPostType
{
    public function __construct(  )
    {
        $this->create_tasks_post_type();
    }

    public function create_tasks_post_type(  )
    {
        $labels = array(
            'name'                => _x( 'Tâches', 'Post Type General Name', 'tasks'),
            'singular_name'       => _x( 'Tâche', 'Post Type Singular Name', 'tasks'),
            'menu_name'           => __( 'Tâche', 'tasks'),
            'all_items'           => __( 'Toutes les tâches', 'tasks'),
            'view_item'           => __( 'Voir toutes les Tâche', 'tasks'),
            'add_new_item'        => __( 'Ajouter une nouvelle tâche', 'tasks'),
            'add_new'             => __( 'Ajouter', 'tasks'),
            'edit_item'           => __( 'Éditer', 'tasks'),
            'update_item'         => __( 'Modifier', 'tasks'),
            'search_items'        => __( 'Rechercher', 'tasks'),
            'not_found'           => __( 'Aucun résultat', 'tasks'),
            'not_found_in_trash'  => __( 'Aucun résultat dans la corbeille', 'tasks'),
        );

        $args = array(
            'label'               => __( 'Tâche', 'tasks'),
            'description'         => __( 'Tâche', 'tasks'),
            'labels'              => $labels,
            'supports'            => array( 'title' ),
            'show_in_rest'        => true,
            'hierarchical'        => false,
            'public'              => true,
            'has_archive'         => true,
            'rewrite'			  => array( 'slug' => 'tasks-post'),

        );

        register_post_type( 'tasks', $args );
    }
}