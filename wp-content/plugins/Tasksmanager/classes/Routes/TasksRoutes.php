<?php


namespace Tasksmanager\Routes;
use Tasksmanager\Database\DataStore;
class TasksRoutes
{
    private $datastore;

    /**
     * TaskRoutes constructor.
     */
    public function __construct()
    {
        $this->create_tasks_routes();
        $this->datastore = new DataStore();
    }

    public function create_tasks_routes()
    {
        //GET task with ID
        register_rest_route('tasksmanager/v0', '/tasks/(?P<id>\d+)', array(
            'methods' => 'GET',
            'callback' => [$this,"get_tasks_with_id"],
        ));

        //GET all tasks
        register_rest_route('tasksmanager/v0', '/tasks', array(
            'methods' => 'GET',
            'callback' => [$this,"get_all_tasks"],
        ));

        //Create tasks -> POST
        register_rest_route('tasksmanager/v0', '/tasks', array(
            'methods' => 'POST',
            'callback' => [$this,"create_tasks"],
        ));

        //Update tasks -> PUT
        register_rest_route('tasksmanager/v0', '/tasks/(?P<id>\d+)', array(
            'methods' => 'PUT',
            'callback' => [$this,"update_tasks"],
        ));

        //Delete tasks -> DELETE
        register_rest_route('tasksmanager/v0', '/tasks/(?P<id>\d+)', array(
            'methods' => 'DELETE',
            'callback' => [$this,"delete_tasks"],
        ));
    }

    public function get_tasks_with_id(\WP_REST_Request $request)
    {
        //Get the id for the task and return the given task
        $args = array(
            'ID' => $request['id'],
            'post_type'	 => 'tasks',
            // 'post_status'	 => 'draft',
            'posts_per_page' => -1
        );
       
        $query=get_post($request['id']);
        return rest_ensure_response($query);
    }

    public function get_all_tasks()
    {
        $args = array(
            'post_type'	 => 'tasks',
            'post_status'	 => 'draft' || 'publish',
            'posts_per_page' => -1
        );
        $query = new \WP_Query( $args );
        return rest_ensure_response($query->posts);
    }   

    public function create_tasks( \WP_REST_Request $request )
    {

        $args = [
            'post_type'	 => 'tasks',
            'post_status' => $request['post_status'],
            'post_title' => $request['post_title'],
            'post_author' => $request['post_author']
            
        ];
        $id=wp_insert_post( $args );

        $this->datastore->save_tasks_data_to_database($id);
        return rest_ensure_response(json_encode($args));        
    }

    public function update_tasks( \WP_REST_Request $request )
    {
        $args = [
            'ID' => $request['id'],
            'post_status' => $request['post_status'],
            'post_title' => $request['post_title'],
            'post_author' => $request['post_author']
        ];

        wp_update_post( $args );
        return rest_ensure_response(json_encode($args));
    }

    public function delete_tasks(\WP_REST_Request $request )
    {
        $postid = $request['id'];
        wp_delete_post( $postid, false );

        $this->datastore->delete_tasks_data_to_database($post_id);
        return rest_ensure_response(json_encode($postid));
    }
    
    
}