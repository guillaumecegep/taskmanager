<?php


namespace Tasksmanager\Database;


class DataStore
{
    /**
     * DataStore constructor.
     */
    public function __construct() {
        add_action('save_post_tasks', [$this, "save_tasks_data_to_database"]);
        add_action('delete_post_tasks', [$this, "delete_tasks_data_to_database"]);

    }

    public function save_tasks_data_to_database( $id )
    {
        global $wpdb;
        $table_name = $wpdb->prefix . "tasksmanager_tasks";
    
        $wpdb->insert($table_name, array(
            'post_id' => $id,
            'user_id' => 1         
        ),array('%d','%d'));
    }

    public function delete_tasks_data_to_database( $post_id )
    {
        global $wpdb;
        $table_name = $wpdb->prefix . "tasksmanager_tasks";
    
        $wpdb->delete($table_name, array(
            'post_id' => $post_id,
            'user_id' => 1,           
        ),array('%d','%d'));
    }

}