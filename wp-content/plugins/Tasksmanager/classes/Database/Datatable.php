<?php
namespace Tasksmanager\Database;

use Tasksmanager\Database\DataStore;

class Datatable
{
    /**
     * Initialization constructor.
     */
    private $current_db_version;

    public function __construct()
    {
        $this->set_current_db_version();
        
        //VERIFIER LE NOM DE TASKS_DB_VERSION
        if ( get_site_option( 'tasks_db_version' ) != $this->current_db_version ) {
            $this->database_init();
        }
        $this->database_init();
    }

    public function database_init()
    {
        if (empty($this->current_db_version)) {
            global $wpdb;
            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
   

            $sql = "CREATE TABLE IF NOT EXISTS `" . $wpdb->prefix . "tasksmanager_tasks`(
                    ID bigint(20) NOT NULL auto_increment,
                    post_id varchar(255) default NULL,
                    user_id int(11) default NULL,
                    PRIMARY KEY  (`ID`)
            );";
           

            dbDelta( $sql ); 
            update_option( "tasks_db_version", $this->current_db_version );
        }
    }

    public function set_current_db_version(){
        $this->current_db_version = get_option('tasks_db_version', 0);
    }
}