<?php
/**
 * Plugin name: TasksManager
 * Plugin URI: 
 * Description: Gestionnaire de tâches
 * Version: 0.1
 * Author: Maxime Gendron, Steven Drouin et Guillaume Ernst
 * Author URI:
 */

require __DIR__ . '/vendor/autoload.php';

use Tasksmanager\Database\DataStore;
use Tasksmanager\Database\Datatable;
use Tasksmanager\CustomPost\TasksPostType;
use Tasksmanager\Fields\Metaboxes;
use Tasksmanager\Routes\RouteManager;
use Tasksmanager\Routes\TasksRoutes;


class Tasksmanager
{
    /**
     * Tasksmanager constructor.
     * @usage Add all needed classes initialization.
     */
    public function __construct()
    {
        add_action( 'init', [ $this, 'init' ] );
        add_action( 'rest_api_init', [ $this, 'rest_init' ] );
        register_activation_hook( __FILE__, [ $this, 'init_data' ] );
    }

    public function init()
    {
        new TasksPostType();
        new Metaboxes();
        new DataStore();       
        new RouteManager();
    }

    public function rest_init(){
        new TasksRoutes('tasksmanager/v0', 'v0', 'tasks' );        
    }
    /**
     * Création de la table des tâches
     */
    public function init_data()
    {
        new Datatable();
    }
}

new Tasksmanager();